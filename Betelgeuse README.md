# Orionx.betelgeuse

---

## Code style 

[Google Styleguide](https://github.com/google/styleguide)

## Design

Core Services:

We need to function in a module. E.g XXSDK.framework.

##### Puplic （GitHub）

1. HTTP [AFNetworking](https://github.com/AFNetworking/AFNetworking)
3. JSON  to model [MJExtension](https://github.com/CoderMJLee/MJExtension)
4.  Refresh [MJRefresh](https://github.com/CoderMJLee/MJRefresh)
5. Masonry [Masonry](https://github.com/SnapKit/Masonry)
5. MSWeakTimer in private framework. [MSWeakTimer](https://github.com/mindsnacks/MSWeakTimer)

##### Private （Our）

1. TouchCellKit 
2. SecurityKit [Safe-guard](https://github.com/Yueze/Safe-guard)

